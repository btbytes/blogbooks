---
title: blogbook
---

OBJECTIVE: To make a blog book out of
`http://normaldeviate.wordpress.com/`.

Get a list of the archive pages:

 * view source of the index page
 * note down the `id` of the archive page links -- `#archive-2`

Use [pup](https://github.com/EricChiang/pup) to extract the URLs.

    curl  http://normaldeviate.wordpress.com/ | pup li#archives-2 'a[href'] attr{href} > archivepages.txt

Inside each of these URLs the Posts are available under
`h3.entry-title > a`:

    curl normaldeviate.wordpress.com/2012/07/ | pup h3.entry-title 'a[href]' attr{href}

We will use `GNU parallel` to print all the links:

    cat archivepages.txt| parallel curl {1} | pup h3.entry-title 'a[href]' attr{href} > all-links.txt

use a small python script to generate a bash script which will then be used to download the HTML files.

~~~~{.python}
# proc1.py
import string

with open('all-links.txt', 'r') as f:
    lines = f.readlines()
    for l in lines:
        url = l.strip()
        tmp = url.strip('/')
        fname = string.replace(tmp, 'http://normaldeviate.wordpress.com/', '')
    fname = string.replace(fname, '/', '-')
        fname = '%s.1' % (fname, )
        print "wget %s -O %s " % (url,fname)
~~~~

Run this script:

    python proc1.py > download.sh

And then run the shell script:

    bash download.sh


Inside individual posts:

- entry title -- `h2.entry-title`
- entry-content -- `div.entry-content`

Comments are inside `id="comments-list"`. Each comment has an id = `id="comment-nn"`



Challenges:

- convert math equations that are rendered using wordpress into images
to LaTex Equations. The `"alt"` and `"src"` attributes contain the
original LaTeX expression. We will have to write a converter back to
latex..


List of all images:

    ls -1 *.1 | xargs -I{} cat {} | pup  '[class*=wp-image]' attr{src} | cut -d\? -f 1 > allimages.txt


// 2014-09-17
