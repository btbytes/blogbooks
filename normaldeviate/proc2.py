#!/usr/bin/python -tt
# -*- coding: utf-8 -*-
from __future__ import print_function

import sys
from bs4 import BeautifulSoup
import base64
import codecs
import md5
import os
import re
import time
import urllib
from HTMLParser import HTMLParser
from bs4 import UnicodeDammit


def remove_node(content, selector):
    nodes = content.select(selector)
    for node in nodes:
        node.replace_with('')
    return content

def clean_image_path(url):
   '''given http://foo.example.com/2013/12/example.png?h=123,
      return <<base64(http://foo.example.com/2013/12/>>example.png
   '''
   m = md5.new()
   m.update(url)
   return '%s.png' % (m.hexdigest(), )

def download_image(img_url, target):
   if not os.path.exists(target):
       urllib.urlretrieve(img_url, target)


def substitute_image(content, selector, target="images", image_path=clean_image_path):
    if not os.path.exists(target):
        os.mkdir(target)
    nodes = content.find_all(class_=selector)
    for node in nodes:
        img_url = node['src']
        target_filename = '%s/%s' % (target, image_path(img_url))
        download_image(img_url, target_filename)
	node['src'] = target_filename
    return content


def replace_latex_links(soup):
    parser = HTMLParser()
    latex_links = soup.find_all(class_="latex")
    for ll  in latex_links:
        alt = ll['alt']
	alt = parser.unescape(alt)
        ll.replace_with('$%s$' % (alt, ))
    return soup



with codecs.open(sys.argv[1], encoding='utf-8', mode='r') as f:
    soup = BeautifulSoup(f)
    title = soup.select('h2.entry-title')[0].text
    title = title.encode('utf-8')
    print('<h1 class="article-title">%s</h1>' % (title, ))
    print('<p class="article-date">%s</p>' % (time.strftime('%a, %d %b %Y', time.strptime(sys.argv[1][:10], '%Y-%m-%d')), ))
    soup = replace_latex_links(soup)
    content = soup.select('div.entry-content')[0]
    content = remove_node(content, 'div.wpcnt')
    content = remove_node(content, 'div.sharedaddy')
    content = remove_node(content, 'script')
    content = remove_node(content, 'img.')
    content = substitute_image(content, re.compile('^wp-image'))
    #content = substitute_image(content, re.compile('^latex'))
    print(content)
