from __future__ import print_function

import sys
import urllib as ul
from BeautifulSoup import BeautifulSoup
from HTMLParser import HTMLParser
parser = HTMLParser()


def main(s):
    s1 = ul.unquote_plus(s)
    print(s1)
    s2 = BeautifulSoup(s1, convertEntities=BeautifulSoup.HTML_ENTITIES)
    print (s2)
    s3 = parser.unescape(s1)
    print (s3)


if __name__ == '__main__':
    s = '%7BX_1%5E%2A%2C%5Cldots%2C+X_n%5E%2A%7D&#038;'
    s = '&#92;displaystyle  X^* = X - &#92;frac{1}{&#92;sqrt{n}}(X_J - X_J^*). '
    main(s)
