Blog Books
==========

An effort to preserve valuable blogs of technical, mathematical and scientific nature as well
formatted books using shell scripts, pandoc, TeX.
